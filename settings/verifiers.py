import json
import os

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.urls import reverse
from django.utils import timezone

import urllib
import urllib.request as urllib2


CAPTCHA_URL = 'https://www.google.com/recaptcha/api/siteverify'
GOOGLE_RECAPTCHA_SECRET_KEY = ''

from django.conf import settings
################################
# Validar Recaptcha con urllib #
################################
def ValidateRecaptcha(value):
    params = {
        'secret': GOOGLE_RECAPTCHA_SECRET_KEY,
        'response': value,
    }
    data = urllib.parse.urlencode(params)
    resp = urllib2.urlopen(
        urllib2.Request(
            CAPTCHA_URL,
            data.encode('utf8')
        )
    )
    gresult = json.loads(resp.read().decode('utf-8'))
    if gresult['success'] is True:
        if gresult['score'] <= 0.6:
            raise ValidationError('Error al validar el captcha, intenta nuevamente más tarde.')
    else:
        raise ValidationError(
            '''
                Error al comunicar con el servidor,
                intente enviar su información nuevamente.
            '''
        )

