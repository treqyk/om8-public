from django import forms
from django.core.validators import RegexValidator
from settings.verifiers import ValidateRecaptcha
from django.utils.translation import gettext_lazy as _


phone_validator = RegexValidator(
    regex=r'^[0-9]{10}$',
    message="El número telefónico debe ser numérico y de 10 Digitos"
)


class ContactForm(forms.Form):
    g_recaptcha_response = forms.CharField(
        label="",
        widget=forms.HiddenInput(),
        required=True,
        validators=[ValidateRecaptcha]
    )

    contact_name = forms.CharField(
        label=_('Nombre Completo *'),
        required=True,
        max_length=15,
        min_length=4,
        widget=forms.TextInput(attrs={
            'placeholder':'Nombre Completo *'}
            )
        )
    contact_email = forms.EmailField(
        label=_('Email de Contacto *'),
        required=True,
        max_length=50,
        min_length=8,
        widget=forms.TextInput(attrs={
            'placeholder':'Email de Contacto'}
            )
        )
    
    contact_phone = forms.CharField(
        label=_('Teléfono *'),
        required=True,
        max_length=10,
        min_length=10,
        validators=[phone_validator],
        widget=forms.TextInput(attrs={
            'placeholder':'Teléfono *'}
            )
        )
    subject = forms.CharField(
        label=_('Asunto'),
        required=True, 
        widget=forms.TextInput(attrs={
            'placeholder':'Asunto',
            'class': 'form-full form-control'}) )
    content = forms.CharField(
        label=_('Mensaje *'),
        required=True, 
        min_length=4,
        max_length=2000, 
        widget=forms.Textarea(attrs={
            'placeholder':'Mensaje *',
            'class': 'form-control'}))

    
    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
