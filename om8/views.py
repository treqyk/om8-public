from django.shortcuts import render
from . import forms
import json, requests
from django.contrib import messages
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import get_template
from django import template
from django.utils.translation import gettext_lazy as _

# Create your views here.
def home(request):
    if request.method == 'POST':
        form = forms.ContactForm(request.POST or None)
        mess = _('Gracias! Tu mensaje ha sido enviado, nos pondremos en contacto contigo.')
        subject = _('Formulario de Contacto')
        
        errors_dict = []
        ajax_response = False
        if form.is_valid():
            
            template = get_template(
                'emails/contact.html'
                )
            content = template.render(locals())
            
            var = requests.post(
                settings.MAIL_URL,
                auth=('api', settings.MAIL_KEY),
                data={
                    'from': settings.MAIL_FROM,
                    'to': [settings.MAIL_TO],
                    'h:Reply-To': '{} <{}>'.format(
                        request.POST.get('contact_name', ''),
                        request.POST.get('contact_email', '')
                    ),
                    'subject': '{} - {}'.format(
                        subject,
                        request.POST.get('subject', '')
                    ),
                    'html': content
                }
            )
            ajax_response = True
            messages.success(
                request,
                mess
            )
        elif form['g_recaptcha_response'].errors:
            result = {
                'form_id': form['g_recaptcha_response'].auto_id,
                'error': form['g_recaptcha_response'].errors
            }
            errors_dict.append(result)
        else:
            for data in form.errors:
                result = {
                    'form_id': form[data].auto_id,
                    'error': form[data].errors
                }
                errors_dict.append(result)
        to_json = {
            'ajax_response': ajax_response,
        }
        json_dump = json.dumps(
            {
                'to_json': to_json,
                'errors_dict': errors_dict
            },
            indent=3
        )
        return HttpResponse(json_dump, content_type='application/json')
    else:
        form = forms.ContactForm()
    return render(request, 'home.html', locals())